= Introduction to TDD
:source-highlighter: rouge
:icons: font
:toc: left

== Outline of the workshop

. Introductions
. What we are going to learn
. Confirm setup of each workstation
. Format of engaging — how to ask questions, breaks, progress reporting, breakouts
. What is TDD?
. Test lists — describe a scenario and ask for feedback on tests to be written, ask what tests to start with
. Demo of TDD flow using the Reverse string exercise
. *BREAK*
. Describe the Stack exercise — ask for the test lists
. Demo the first test
. Write the rest of the tests
. Review
. *BREAK*
. Introduce some test practices
. Describe string calculator as a set of requirements — positive numbers only
. Exercise to write positive number string calculator
. Introduce negative numbers as next exercise
. Introduce configurable separators as last exercise
. *Review*
. Explain why TDD is important
. Ask for workshop feedback


== What is Test Driven Development?

****
TDD is a set of programming practices whereby we write another program to test our program.
The test program consists of lots of small functions that test every bit of functionality of the main program.
We use TDD to gradually evolve a program whilst ensuring that it is functioning as expected.
****

== Why is TDD important?

* It creates a safety net when we change code
* It gives us confidence that our programs work as expected
* It is a technique that helps us evolve a design in small steps and encourages better design
* It encourages us to write just the right amount of code to solve a problem

== What is the TDD process?

. Write a test that fails
. Make the test pass by writing just enough of your program
. Improve the code while ensuring the tests do not fail

[NOTE]
====
[red]#*RED*# — [green]*GREEN* — [yellow]*REFACTOR*
====

== Getting started with Python

=== The Typical Structure of a Python Test

[source,python]
----
include::./test_structure.py[]
----
<1> Import unit testing libraries
<2> Create the class for the test functions
<3> The test function
<4> The actual test which checks that the sum of 1 and 2 is 3
<5> The test runner

Run using `python3 -m unittest <path/testfile.py>`

.Extra Reading Material
****
These articles provides basic information about how to do unit testing in Python. You can also use the existing TestCase skeletons in the exercise directories for inspiration.

https://realpython.com/python-testing/[Getting Started With Testing in Python]

https://docs.python.org/3/library/unittest.html[Python unittest]

https://docs.python-guide.org/writing/tests/[Testing your code]
****

== Technique #1: Test lists

. When you start a programming cycle, make a list of the tests you need *think* you need.
. Write it down! — in any order.
. Then pick one test — often it is the test that is least scary to write.
. Try to write the test and get it to pass.
. If you can't get it to pass, leave to once side and pick another test from your list.
. Once you complete a test, review your test list — strike off irrelevant tests, add on new tests
. If you are in a middle of a test and discover that you need another test, just add it too your test list

=== Practice Test lists

Let's assume we need to write a program to reverse a string. For example, if we give the function `abcde` then it will return `edcba`.
Write down, in any order, what are the tests that you need to write to make the function work under all conditions.
Think about the different strings that you could give to the function and what you expect the function to return.

In such an exercise there are no write or wrong answers. As you write each test, you will realise whether the test is useful or not.

[%collapsible]
.*Click here to see an example test list for reversing a string*
====
. Reverse `abcde` should return `edcba`
. Reverse empty string should return empty string
. Reverse None
. Reverse single character string `a` should be `a`
. Reverse repeated character string `aaa` should be `aaa`
====

== Technique #2: Fake it

When starting to write a new test, fake the implementation of the function with hardcoded values.

[source,python]
----
include::./test_fakeit_green.py[]
----
<1> Call the function to test with a few hardcoded values
<2> Return a hardcoded value - the fake - because `3` is the answer of `1 + 2`


This seems counter-intuitive but it exists to confirm that your function does work for as expect for that faked value.
Thereafter, when we get to the refactor stage, we can replace the fake value with the actual code to make the test work.

Once the test passes with the fake, leave the test alone and change the function to get rid of the fake
[source,python]
----
include::./test_fakeit_refactored.py[]
----
<1> The test function is left unchanged
<2> Replace the fake with the actual logic

=== Practice Faking

Write at test to reverse the string `hello`.

. First write a test to call a function to reverse a string with value `hello`
. Write the reverse function but fake the return value with `""` - an empty string
. [red]*RED STAGE*: Run the test, and it should fail because the empty string returned is not the reversed string `olleh`
. [green]*GREEN STAGE*: Change the reverse function to return the fake `olleh` and run the test again. It should pass.
. [yellow]*REFACTOR STAGE*: Now change the reverse function to remove the fake and actually do the string reversal
. [green]*GREEN STAGE*:Run the test and it should still pass

== Properties of tests

From https://medium.com/@kentbeck_7670/test-desiderata-94150638a4b3[Test Desiderata] and this https://www.youtube.com/playlist?list=PLlmVY7qtgT_lkbrk9iZNizp978mVzpBKl[set of videos].

=== Isolated

Tests should return the same results regardless of the order in which they are run.

video::HApI2cspQus[youtube, height=320]

=== Composable

If tests are isolated, then I can run 1 or 10 or 100 or 1,000,000 and get the same results.

video::Wf3WXYaMt8E[youtube, height=320]

=== Fast

Tests should run quickly.

video::L0dZ7MmW6xc[youtube, height=320]

=== Inspiring

Passing the tests should inspire confidence

video::2Q1O8XBVbZQ[youtube, height=320]

=== Writable

Tests should be cheap to write relative to the cost of the code being tested.

video::CAttTEUE9HM[youtube, height=320]

=== Readable

Tests should be comprehensible for reader, invoking the motivation for writing this particular test.

video::bDaFPACTjj8[youtube, height=320]

=== Behavioral

Tests should be sensitive to changes in the behavior of the code under test. If the behavior changes, the test result should change.

video::5LOdKDqdWYU[youtube, height=320]

=== Structure-insensitive

Tests should not change their result if the structure of the code changes.

video::bvRRbWbQwDU[youtube, height=320]

=== Automated

Tests should run without human intervention.

video::YQlmP08dj6g[youtube, height=320]

=== Specific

If a test fails, the cause of the failure should be obvious.

video::8lTfrCtPPNE[youtube, height=320]

=== Deterministic

If nothing changes, the test result shouldn’t change.

video::PwWyp-wpFiw[youtube, height=320]

=== Predictive

If the tests all pass, then the code under test should be suitable for production.

video::7o5qxxx7SmI[youtube, height=320]

== Techniques

=== Test Lists

When you start a programming cycle, make a list of the tests you need *think* you need.

Write it down! — in any order.

Then pick one test — often it is the test that is least scary to write.

Try to write the test and get it to pass.

If you can't get it to pass, leave to once side and pick another test from your list.

Once you complete a test, review your test list — strike off irrelevant tests, add on new tests

If you are in a middle of a test and discover that you need another test, just add it too your test list

=== Faking

To get a test going, do the bare minimum to make it pass by hard coding values.

Sounds strange but the fake values force you to remain focused on the interface (the API) that is described in the test.

The fakes force you to consider the kind of data that your program handles.

Use meaningful values for your fakes.

=== Start with the assertion

Sometimes it is hard to figure out what to test — work backwards from the output of the test as an assertion.

Then write what you need to lead up to the assertion.

=== DRY
----
Don't Repeat Yourself
----

In the refactor phase, remove all duplication — hardcoded values and code in tests and the program

=== Triangulation

Sometimes we don't know how to get a test to pass because the concept or requirement being tested is complex.

Write a part of the complex part with a single small test.

Then write another test that adds in a little more complexity.

Continue until you have a suite of small tests that fleshes out the full functionality in that single requirement.

=== Child tests

Sometimes we write a test and realise it is larger than we planned.

Leave that test in place as a failing test.

Write *child* tests that deal with the smaller parts.
Once all child tests pass, the parent test should pass automatically.

=== Name the test later

Naming of test is really important.

It reveals the intention of the test and is a statement of the requirement

Sometimes we only what it is specifically after we write the test — then name the test to be a lot more clear and precise.

== Exercises

=== Before you start

==== Setup
In order to setup this repository to use for tracking progress on your workshop exercises, you need to:

. https://gitlab.com/help/user/project/repository/forking_workflow.md[Fork this repository] from the Gitlab repo
. Ensure you setup git with your student email address using `git config --global user.email "<your@email.address>"`
  The script that posts results to the workshop leaderboard will use the `user.email` config value for `git`.
. Run the `setup.sh` script to install dependencies and configure `watchman`


==== Running the tests
There is a directory for each exercise in the workshop, e.g. `exercise001` for the first exercise.
Each of these directories contain a basic structure for the unittests.

To run a unittest for an exercise, do e.g.:
```bash
python3 -m unittest exercise001/test_reverse_string.py
```

==== Cleanup after the workshop
Run `end_workshop.sh` at the end of the workshop to delete the `watchman` triggers on your working directoy.

=== Warm-up

==== Reverse a String

Given string "abcde", reverse the string to produce "edcba"

=== Thinking in tests

==== A simple stack data structure

Start with an empty stack — the size should be 0

Push an item on the stack — the size should be 1

Pop an item from the stack — the item popped should be the last item pushed

Push several items in the stack — the size should == the number of pushes

Pop several items off the stack — the items should be popped in the reverse sequence of the pushes

Popping an empty stack should raise an error

=== Evolving your program with tests

==== String calculator

Write a program that adds up the list of numbers in a string.

Start with an empty string "" — the sum of an empty string is 0

A single digit number "4" — the sum should be 4

A multi-digit number "45" — should be 45

Two numbers separated by a comma "3,5" — should be 8

Three numbers "5,100,4" — should be 109

Single negative number "-2" — should be -2

Multiple numbers with different signs "10,-2,4,-3,5" — should be 16

Different separator "1 3 8" — should be 12

=== Practice! Practice! Practice!

==== Conway's Game of Life

Play it https://bitstorm.org/gameoflife/[here] (instructions included)

==== Othello/Reversi

See https://en.wikipedia.org/wiki/Reversi[Wikipedia] for instructions.

Play the game https://playpager.com/play-reversi/index.html[here].
